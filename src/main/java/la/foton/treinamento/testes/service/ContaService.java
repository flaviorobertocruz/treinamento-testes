package la.foton.treinamento.testes.service;

import java.util.List;
import java.util.Optional;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.inject.Inject;

import la.foton.treinamento.testes.comum.excecao.Mensagem;
import la.foton.treinamento.testes.comum.excecao.NegocioException;
import la.foton.treinamento.testes.dao.ContaDAO;
import la.foton.treinamento.testes.entidade.Cliente;
import la.foton.treinamento.testes.entidade.Conta;
import la.foton.treinamento.testes.entidade.ContaCorrente;
import la.foton.treinamento.testes.entidade.ContaPoupanca;
import la.foton.treinamento.testes.entidade.TipoDaConta;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER) // opcional
public class ContaService {

	@Inject
	private ContaDAO dao;

	@EJB
	private ClienteService clienteService;

	@TransactionAttribute(TransactionAttributeType.REQUIRED) // opcional
	public Conta abreConta(Cliente titular, TipoDaConta tipo) throws NegocioException {
		clienteService.validaSituacaoCliente(titular);

		Conta conta = criaConta(titular, tipo);

		conta.ativa();

		dao.insere(conta);

		return conta;
	}

	public Conta consultaPorNumero(Integer numero) throws NegocioException {
		Optional<Conta> conta = dao.consultaPorNumero(numero);

		if (!conta.isPresent()) {
			throw new NegocioException(Mensagem.CONTA_NAO_ENCONTRADA);
		}

		return conta.get();
	}

	public void encerraConta(Integer numero) throws NegocioException {
		Conta conta = this.consultaPorNumero(numero);

		conta.encerra();

		dao.atualiza(conta);
	}

	public boolean existeContasParaTitular(String cpf) {
		List<Conta> contas = dao.consultaPorTitular(cpf);

		return !contas.isEmpty();
	}

	private Conta criaConta(Cliente titular, TipoDaConta tipo) {
		Conta conta = null;

		if (TipoDaConta.CORRENTE.equals(tipo)) {
			conta = new ContaCorrente();
			((ContaCorrente) conta).setLimiteDoChequeEspecial(500.0);
		} else {
			conta = new ContaPoupanca();
			((ContaPoupanca) conta).setDiaDoAniversario(1);
		}

		conta.setNumero(dao.geraNumero());
		conta.setTitular(titular);

		return conta;
	}

}
