package la.foton.treinamento.testes.service;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;

import la.foton.treinamento.testes.comum.excecao.Mensagem;
import la.foton.treinamento.testes.comum.excecao.NegocioException;
import la.foton.treinamento.testes.dao.ClienteDAO;
import la.foton.treinamento.testes.entidade.Cliente;
import la.foton.treinamento.testes.entidade.SituacaoDoCliente;

@Stateless
public class ClienteService {

	@Inject
	private ClienteDAO dao;

	@EJB
	private ContaService contaService;

	public void validaSituacaoCliente(Cliente cliente) throws NegocioException {
		if (SituacaoDoCliente.PENDENTE.equals(cliente.getSituacao())) {
			throw new NegocioException(Mensagem.SITUACAO_CLIENTE_PENDENTE);
		}
	}

	public Cliente consultaPorCPF(String cpf) throws NegocioException {
		Cliente cliente = dao.consultaPorCPF(cpf);

		if (cliente == null) {
			throw new NegocioException(Mensagem.CLIENTE_NAO_ENCONTRADO);
		}

		return cliente;
	}

	public void cadastraCliente(String cpf, String nome) throws NegocioException {
		if (cpf == null || cpf.isEmpty() || nome == null || nome.isEmpty()) {
			throw new NegocioException(Mensagem.CLIENTE_NAO_PODE_SER_CADASTRADO);
		}

		if (dao.consultaPorCPF(cpf) != null) {
			throw new NegocioException(Mensagem.CLIENTE_JA_CADASTRADO);
		}

		dao.insere(new Cliente(cpf, nome));
	}

	public void ativaCliente(String cpf) throws NegocioException {
		Cliente cliente = this.consultaPorCPF(cpf);

		cliente.ativa();

		dao.atualiza(cliente);
	}

	public void removeCliente(String cpf) throws NegocioException {
		if (contaService.existeContasParaTitular(cpf)) {
			throw new NegocioException(Mensagem.NAO_PODE_EXCLUIR_CLIENTE_QUE_POSSUI_CONTA);
		}

		Cliente cliente = this.consultaPorCPF(cpf);

		dao.remove(cliente);
	}

	public List<Cliente> consultaTodos() throws NegocioException {
		List<Cliente> clientes = dao.consultaTodos();

		if (clientes.isEmpty()) {
			throw new NegocioException(Mensagem.NAO_EXISTEM_CLIENTES);
		}

		return clientes;
	}

}
