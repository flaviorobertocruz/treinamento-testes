package la.foton.treinamento.testes.comum.converter;

import javax.persistence.AttributeConverter;

import la.foton.treinamento.testes.entidade.TipoDaConta;

public class TipoDaContaConverter implements AttributeConverter<TipoDaConta, Integer> {

	@Override
	public Integer convertToDatabaseColumn(TipoDaConta attribute) {
		if (attribute.getChave() == null) {
			throw new IllegalArgumentException("Desconhecido " + attribute);
		}

		return attribute.getChave();
	}

	@Override
	public TipoDaConta convertToEntityAttribute(Integer value) {
		switch (value) {
		case 1:
			return TipoDaConta.CORRENTE;
		case 2:
			return TipoDaConta.POUPANCA;
		default:
			throw new IllegalArgumentException("Desconhecido " + value);
		}
	}

}
