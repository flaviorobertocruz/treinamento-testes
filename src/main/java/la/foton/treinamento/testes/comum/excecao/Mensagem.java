package la.foton.treinamento.testes.comum.excecao;

public enum Mensagem {

	SALDO_INSUFICIENTE("Saldo Insuficiente"), //
	CONTA_JA_ENCERRADA("Conta já encerrada"), //
	CONTA_NAO_PODE_SER_ENCERRADA("Conta não pode ser encerrada por possuir saldo"),
	SITUACAO_CLIENTE_PENDENTE("Situação do cliente está pendente"), //
	CPF_INVALIDO("CPF Inválido"), //
	CONTA_NAO_ENCONTRADA("Conta não encontrada"), //
	CLIENTE_NAO_ENCONTRADO("Cliente não encontrado"), //
	CLIENTE_NAO_PODE_SER_CADASTRADO("Cliente não pode ser cadastrado, dados inválidos"),
	CLIENTE_JA_CADASTRADO("Cliente já cadastrado"), //
	NAO_PODE_EXCLUIR_CLIENTE_QUE_POSSUI_CONTA("Cliente possui conta e não pode ser removido"),
	NAO_EXISTEM_CLIENTES("Não existem clientes");

	private String texto;

	private Mensagem(String texto) {
		this.texto = texto;
	}

	public String getTexto() {
		return texto;
	}

}
