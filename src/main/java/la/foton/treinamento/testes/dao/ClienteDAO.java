package la.foton.treinamento.testes.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import la.foton.treinamento.testes.entidade.Cliente;

public class ClienteDAO {

	@PersistenceContext
	private EntityManager em;

	public void insere(Cliente cliente) {
		em.persist(cliente);
	}

	public void atualiza(Cliente cliente) {
		em.merge(cliente);
	}

	public void remove(Cliente cliente) {
		em.remove(cliente);
	}

	public Cliente consultaPorCPF(String cpf) {
		return em.find(Cliente.class, cpf);
	}

	public List<Cliente> consultaTodos() {
		String sql = "select c from Cliente c";

		return em.createQuery(sql, Cliente.class).getResultList();
	}

}
