package la.foton.treinamento.testes.dao;

import java.util.List;
import java.util.Optional;

import javax.enterprise.inject.Default;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import la.foton.treinamento.testes.entidade.Conta;

@Default
public class ContaDAOImpl implements ContaDAO {

	@PersistenceContext
	private EntityManager em;

	@Override
	public Integer geraNumero() {
		String sql = "select max(c.numero) from Conta c";

		TypedQuery<Integer> query = em.createQuery(sql, Integer.class);

		Integer numero = query.getSingleResult();

		return numero != null ? numero + 1 : 1;
	}

	@Override
	public void insere(Conta conta) {
		em.persist(conta);
	}

	@Override
	public Optional<Conta> consultaPorNumero(Integer numero) {
		return Optional.of(em.find(Conta.class, numero));
	}

	@Override
	public void atualiza(Conta conta) {
		em.merge(conta);
	}

	@Override
	public List<Conta> consultaPorTitular(String cpf) {
		String sql = "select c from Conta c where c.titular.cpf = :cpf";

		TypedQuery<Conta> query = em.createQuery(sql, Conta.class);

		query.setParameter("cpf", cpf);

		return query.getResultList();
	}

}
