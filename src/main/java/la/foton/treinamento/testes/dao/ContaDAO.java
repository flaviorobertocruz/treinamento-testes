package la.foton.treinamento.testes.dao;

import java.util.List;
import java.util.Optional;

import la.foton.treinamento.testes.entidade.Conta;

public interface ContaDAO {

	Integer geraNumero();
	
	void insere(Conta conta);

	public Optional<Conta> consultaPorNumero(Integer numero);
	
	void atualiza(Conta conta);

	List<Conta> consultaPorTitular(String cpf);

}
