package la.foton.treinamento.testes.entidade;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;

import la.foton.treinamento.testes.comum.converter.TipoDaContaConverter;
import la.foton.treinamento.testes.comum.excecao.Mensagem;
import la.foton.treinamento.testes.comum.excecao.NegocioException;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class Conta {

	@Id
	private Integer numero;

	@Column
	private Integer agencia;

	@ManyToOne
	@JoinColumn(name = "cpf")
	private Cliente titular;

	@Column
	protected Double saldo;

	@Column
	@Enumerated(EnumType.ORDINAL)
	private EstadoDaConta estado;

	@Column
	@Convert(converter = TipoDaContaConverter.class)
	protected TipoDaConta tipo;

	@PrePersist
	public void prePersist() {
		this.agencia = 1234;
	}

	public Conta() {
		this.saldo = 0.0;
	}

	public void credita(Double valor) {
		saldo = saldo + valor;
	}

	public abstract void debita(Double valor) throws NegocioException;

	public void transfere(Double valor, Conta contaDestino) throws NegocioException {
		// Template Method
		this.debita(valor);

		contaDestino.credita(valor);
	}

	public void encerra() throws NegocioException {
		if (this.saldo > 0) {
			throw new NegocioException(Mensagem.CONTA_NAO_PODE_SER_ENCERRADA);
		}

		if (EstadoDaConta.ENCERRADA.equals(estado)) {
			throw new NegocioException(Mensagem.CONTA_JA_ENCERRADA);
		}

		encerrada();
	}

	public void encerrada() {
		this.estado = EstadoDaConta.ENCERRADA;
	}

	public void ativa() {
		this.estado = EstadoDaConta.ATIVA;
	}

	public Integer getNumero() {
		return numero;
	}

	public void setNumero(Integer numero) {
		this.numero = numero;
	}

	public Integer getAgencia() {
		return agencia;
	}

	public Cliente getTitular() {
		return titular;
	}

	public void setTitular(Cliente titular) {
		this.titular = titular;
	}

	public Double getSaldo() {
		return saldo;
	}

	public EstadoDaConta getEstado() {
		return estado;
	}

	public TipoDaConta getTipo() {
		return tipo;
	}

}
