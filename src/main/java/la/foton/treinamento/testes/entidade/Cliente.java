package la.foton.treinamento.testes.entidade;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;

import la.foton.treinamento.testes.comum.excecao.Mensagem;
import la.foton.treinamento.testes.comum.excecao.NegocioException;
import la.foton.treinamento.testes.comum.helper.CPFHelper;

@Entity
@Table
public class Cliente {

	@Id
	@Column
	private String cpf;

	@Column(nullable = false)
	private String nome;

	@Column
	@Enumerated(EnumType.ORDINAL)
	private SituacaoDoCliente situacao;

	public Cliente() {
		this.situacao = SituacaoDoCliente.PENDENTE;
	}

	public Cliente(String cpf, String nome) throws NegocioException {
		this.setCpf(cpf);
		this.nome = nome;

		this.situacao = SituacaoDoCliente.PENDENTE;
	}

	public void setCpf(String cpf) throws NegocioException {
		if (!CPFHelper.isCpfValido(cpf)) {
			throw new NegocioException(Mensagem.CPF_INVALIDO);
		}

		this.cpf = cpf;
	}

	public String getCpf() {
		return cpf;
	}

	public String getNome() {
		return nome;
	}

	public SituacaoDoCliente getSituacao() {
		return situacao;
	}

	public void ativa() {
		this.situacao = SituacaoDoCliente.ATIVO;
	}

}
