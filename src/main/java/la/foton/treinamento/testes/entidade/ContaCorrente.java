package la.foton.treinamento.testes.entidade;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import la.foton.treinamento.testes.comum.excecao.Mensagem;
import la.foton.treinamento.testes.comum.excecao.NegocioException;

@Entity
@Table(name = "ContaCorrente")
public class ContaCorrente extends Conta {

	@Column(name = "limiteDoChequeEspecial")
	private Double limiteDoChequeEspecial;

	public ContaCorrente() {
		this.tipo = TipoDaConta.CORRENTE;
		this.setLimiteDoChequeEspecial(0.0);
	}

	@Override
	public void debita(Double valor) throws NegocioException {
		if (saldo + getLimiteDoChequeEspecial() < valor) {
			throw new NegocioException(Mensagem.SALDO_INSUFICIENTE);
		}

		saldo = saldo - valor;
	}

	public Double getLimiteDoChequeEspecial() {
		return limiteDoChequeEspecial;
	}

	public void setLimiteDoChequeEspecial(Double limiteDoChequeEspecial) {
		this.limiteDoChequeEspecial = limiteDoChequeEspecial;
	}

}
