package la.foton.treinamento.testes.service;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.Matchers.hasSize;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;

import la.foton.componente.openejbrunner.OpenEjbRunner;
import la.foton.componente.openejbrunner.annotation.DatabaseInit;
import la.foton.componente.openejbrunner.annotation.InjectCdi;
import la.foton.componente.openejbrunner.annotation.InjectEjb;
import la.foton.treinamento.testes.comum.excecao.NegocioException;
import la.foton.treinamento.testes.dao.ClienteDAO;
import la.foton.treinamento.testes.entidade.Cliente;
import la.foton.treinamento.testes.entidade.SituacaoDoCliente;

@RunWith(OpenEjbRunner.class)
@DatabaseInit
public class ClienteServiceTest {

	@InjectEjb("ClienteServiceLocalBean")
	private ClienteService service;

	@InjectCdi
	private ClienteDAO dao;

	@Test
	public void deveConsultarTodosClientes() {
		try {
			List<Cliente> clientes = service.consultaTodos();

			assertThat(clientes, hasSize(2));
			assertThat(clientes.get(0).getNome(), equalTo("Flavio Roberto"));
			assertThat(clientes.get(0).getCpf(), equalTo("77276469115"));
			assertThat(clientes.get(0).getSituacao(), equalTo(SituacaoDoCliente.ATIVO));
			assertThat(clientes.get(1).getNome(), equalTo("Erica Jordana"));
			assertThat(clientes.get(1).getCpf(), equalTo("02728594430"));
			assertThat(clientes.get(1).getSituacao(), equalTo(SituacaoDoCliente.PENDENTE));
		} catch (NegocioException e) {
			fail(e.getMessage());
		}
	}
	
	@Test
	public void deveConsultarClientePorCpf() {
		try {
			Cliente cliente = service.consultaPorCPF("02728594430");

			assertNotNull(cliente);
			assertThat(cliente.getNome(), equalTo("Erica Jordana"));
			assertThat(cliente.getSituacao(), equalTo(SituacaoDoCliente.PENDENTE));
		} catch (NegocioException e) {
			fail(e.getMensagem().getTexto());
		}
	}

}
