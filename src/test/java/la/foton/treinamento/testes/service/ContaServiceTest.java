package la.foton.treinamento.testes.service;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doThrow;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import la.foton.treinamento.testes.comum.excecao.Mensagem;
import la.foton.treinamento.testes.comum.excecao.NegocioException;
import la.foton.treinamento.testes.dao.ContaDAO;
import la.foton.treinamento.testes.entidade.Cliente;
import la.foton.treinamento.testes.entidade.Conta;
import la.foton.treinamento.testes.entidade.ContaCorrente;
import la.foton.treinamento.testes.entidade.EstadoDaConta;
import la.foton.treinamento.testes.entidade.TipoDaConta;

@RunWith(MockitoJUnitRunner.class)
public class ContaServiceTest {

	@InjectMocks
	private ContaService service;

	@Mock
	private ContaDAO dao;

	@Mock
	private ClienteService clienteService;

	private Cliente titular;

	@Before
	public void setUp() throws NegocioException {
		titular = new Cliente("77276469115", "Flavio Roberto");
	}

	@Test
	public void deveAbrirUmaContaCorrente() {
		Conta contaAberta = null;

		// Given - Dado que o titular está ativo
		given(dao.geraNumero()).willReturn(1);
		titular.ativa();

		// When
		try {
			contaAberta = service.abreConta(titular, TipoDaConta.CORRENTE);
		} catch (NegocioException e) {
			fail(e.getMessage());
		}

		// Then
		assertNotNull(contaAberta);
		assertThat(contaAberta.getNumero(), equalTo(1));
		assertThat(contaAberta.getTipo(), equalTo(TipoDaConta.CORRENTE));
		assertThat(contaAberta.getEstado(), equalTo(EstadoDaConta.ATIVA));
		assertThat(contaAberta.getSaldo(), equalTo(0.0));
		assertThat(((ContaCorrente) contaAberta).getLimiteDoChequeEspecial(), equalTo(500.00));
	}

	@Test
	public void naoDeveAbrirUmaContaParaUmTitularPendente() {
		try {
			// Given - dado que o titular está pendente
			doThrow(new NegocioException(Mensagem.SITUACAO_CLIENTE_PENDENTE)) //
					.when(clienteService) //
					.validaSituacaoCliente(titular);

			// When
			service.abreConta(titular, TipoDaConta.CORRENTE);

			fail();
		} catch (NegocioException e) {
			// Then
			assertThat(e.getMensagem(), equalTo(Mensagem.SITUACAO_CLIENTE_PENDENTE));
		}
	}

}
