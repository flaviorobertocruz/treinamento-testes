drop table ContaPoupanca if exists;
drop table ContaCorrente if exists;
drop table Conta if exists;
drop table Cliente if exists;

create table Cliente (
	cpf varchar(255) not null,
	nome varchar(255) not null,
	situacao integer,
	primary key (cpf)
);

create table Conta (
	numero integer not null,
	agencia integer,
	estado integer,
	saldo double,
	tipo integer,
	cpf varchar(255),
	primary key (numero)
);

create table ContaCorrente (
	limiteDoChequeEspecial double,
	numero integer not null,
	primary key (numero)
);

create table ContaPoupanca (
	diaDoAniversario integer,
	numero integer not null,
	primary key (numero)
);

alter table Conta 
	add constraint FK_CONTA_CLIENTE 
	foreign key (cpf) 
	references Cliente;
    
alter table ContaCorrente 
	add constraint FK_CONTA_CORRENTE 
	foreign key (numero) 
	references Conta;
    
alter table ContaPoupanca 
	add constraint FK_CONTA_POUPANCA 
	foreign key (numero) 
	references Conta;